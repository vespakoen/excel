<?php namespace Gosidesign\Excel;

use Gosidesign\Excel\Exceptions\FileNotFoundException;
use Gosidesign\Excel\Exceptions\FileLoadException;

use Exception;

use PHPExcel;
use PHPExcel_IOFactory;

class Excel {

  public $phpexcel;

  public $filePath;

  public function __construct($filePath = null)
  {
    $this->phpexcel = new PHPExcel();
    $this->filePath = $filePath;
  }

  public static function make()
  {
    return new static;
  }

  public static function open($filePath = null)
  {
    return new static($filePath);
  }

  public function toArray()
  {
    $filePath = $this->filePath;

    if( ! $filePath)
    {
      throw new FileNotFoundException('$filePath was not set, use Excel::open(\'path/to/file\')');
    }

    if( ! file_exists( $filePath ))
    {
      throw new FileNotFoundException('The given file ('.$filePath.') was not found.');
    }

    // Read your Excel workbook
    try
    {
      $inputFileType = PHPExcel_IOFactory::identify($filePath);
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($filePath);
    }
    catch(Exception $e)
    {
      throw new FileLoadException('Error loading file '.$filePath.': '.$e->getMessage());
    }

    // Get worksheet dimensions
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    // Loop through each row of the worksheet in turn
    for ($row = 1; $row <= $highestRow; $row++){
      // Read a row of data into an array
      $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
      $result[] = $rowData[0];
    }

    return $result;
  }

}
